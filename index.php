<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/global.css">
    <link rel="stylesheet" href="assets/css/admin.css">
    <title>Accueil</title>
</head>
<body>
<header id="header">
        <h1 id="headerTitle">BIOKOP</h1>
        <img src="assets/img/oeilBarre.png" id="eye" alt="Symbole avec un oeil barré destiné au public daltonien">
    </header> 

    <main>

        <section id="sectionTop">
            <h2>ACCUEIL</h2>
            <p>Découvrez nos Fruits & Légumes de saison</p>
        </section>

        <section class="create">
            <form action="" method="POST">
                <label for="mois_view">Pour le mois de :</label>
                <select name="mois_view" id="mois">
                    <option value="janvier">janvier</option>
                    <option value="fevrier">fevrier</option>
                    <option value="mars">mars</option>
                    <option value="avril">avril</option>
                    <option value="mai">mai</option>
                    <option value="juin">juin</option>
                    <option value="juillet">juillet</option>
                    <option value="aout">aout</option>
                    <option value="septembre">septembre</option>
                    <option value="octobre">octobre</option>
                    <option value="novembre">novembre</option>
                    <option value="decembre">decembre</option>
                </select>
                <input type="submit" name="displayMonths" value="Afficher">
            </form>
            
        </section>

        <section class="displayMonths">
            <ul>
                    <?php
                    include('view/id.php');

                    // connexion base de donnée via PDO
                    $bdd = new PDO('mysql:host=sql202.byetcluster.com;dbname=b22_26110660_biokop;charset=UTF8', 'b22_26110660', 'N94jLk5c');
                    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

                    $moisView = $_POST['mois_view'];
                    
                    $sql = "SELECT id_fl, nom, mois FROM `TBLfruitEtLegume` WHERE mois = '$moisView'";

                    // Affichage des légumes sous forme de tableau pour le mois demandé
                    foreach ($bdd->query($sql) as $row) { ?>
                        <li><?php print $row['nom'] ?></li>
                    <?php } ?>
                <br>
            </ul>
        </section>
    </main>
    
    <?php include('view/footer.php') ?>
    <script src="view/daltonien.js"></script>
</body>
</html>