-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 22, 2020 at 09:34 AM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BDDbiokop`
--

-- --------------------------------------------------------

--
-- Table structure for table `TBLfruitEtLegume`
--

CREATE TABLE `TBLfruitEtLegume` (
  `id_fl` mediumint(8) UNSIGNED NOT NULL,
  `nom` varchar(100) NOT NULL,
  `mois` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TBLfruitEtLegume`
--

INSERT INTO `TBLfruitEtLegume` (`id_fl`, `nom`, `mois`) VALUES
(109, 'celerie', 'janvier'),
(111, 'betterave', 'mars'),
(112, 'betterave', 'janvier'),
(113, 'betterave', 'fevrier'),
(114, 'betterave', 'mars'),
(115, 'betterave', 'mars'),
(116, 'betterave', 'avril'),
(117, 'betterave', 'mai'),
(118, 'betterave', 'juin'),
(119, 'betterave', 'juillet'),
(120, 'betterave', 'aout'),
(121, 'betterave', 'aout'),
(122, 'betterave', 'septembre'),
(123, 'betterave', 'octobre'),
(124, 'betterave', 'octobre'),
(125, 'betterave', 'octobre'),
(126, 'betterave', 'novembre'),
(127, 'betterave', 'decembre'),
(128, 'betterave', 'janvier');

-- --------------------------------------------------------

--
-- Table structure for table `TBLuser`
--

CREATE TABLE `TBLuser` (
  `id_user` tinyint(4) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(164) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TBLuser`
--

INSERT INTO `TBLuser` (`id_user`, `email`, `pass`) VALUES
(3, 'paulette@biokop.fr', '$2y$10$CsXR7yKqfrO2pdG/iCloh.2vF404rolcFXgIhpJR51JBjYTAQAxZ6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `TBLfruitEtLegume`
--
ALTER TABLE `TBLfruitEtLegume`
  ADD PRIMARY KEY (`id_fl`);

--
-- Indexes for table `TBLuser`
--
ALTER TABLE `TBLuser`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `TBLfruitEtLegume`
--
ALTER TABLE `TBLfruitEtLegume`
  MODIFY `id_fl` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `TBLuser`
--
ALTER TABLE `TBLuser`
  MODIFY `id_user` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
