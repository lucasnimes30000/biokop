<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/global.css">
    <link rel="stylesheet" href="../assets/css/login.css">
    <title>Connexion</title>
</head>
<body>
    <?php include('header.php') ?>

    <main>

        <section id="sectionTop">
            <h2>ADMINISTRATION</h2>
            <a class="redirection" href="../index.php">Redirection Site Web</a>
        </section>

        <section class="loginSection">
            <h3 id="loginTitle">Connexion</h3>
            <form action="log.php" class="loginForm" method="POST">
                <input type="email" name="email" placeholder="Adresse mail">
                <input type="password" name="pass" placeholder="Mot de passe">
                <button type="submit">Connexion</button>
            </form>
        </section>

    </main>
    
    <?php include('footer.php') ?>
        <script src="daltonien.js"></script>

</body>
</html>