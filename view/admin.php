<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/global.css">
    <!-- meta description -->
    <link rel="stylesheet" href="../assets/css/admin.css">
    <title>Administration</title>
</head>
<body>
    <?php include('header.php') ?>

    <main>

<?php
    if (isset($_SESSION['email'])) {
        echo 'Connecté avec l\'adresse mail : ' . $_SESSION['email'];
    } else {
        header('Location: log_in.php');
    }
?>
        <section id="sectionTop">
            <a class="redirection" href="../index.php">Redirection Site Web</a>
            <h2>ADMINISTRATION</h2>
            <a href="logout.php">Déconnexion</a>
        </section>

        <section class="create">
            <form action="" method="POST">
                <label for="mois_view">Pour le mois de :</label>
                <select name="mois_view" id="mois">
                    <option value="janvier">janvier</option>
                    <option value="fevrier">fevrier</option>
                    <option value="mars">mars</option>
                    <option value="avril">avril</option>
                    <option value="mai">mai</option>
                    <option value="juin">juin</option>
                    <option value="juillet">juillet</option>
                    <option value="aout">aout</option>
                    <option value="septembre">septembre</option>
                    <option value="octobre">octobre</option>
                    <option value="novembre">novembre</option>
                    <option value="decembre">decembre</option>
                </select>
                <input type="submit" name="displayMonths" value="Afficher">
            </form>
            
        </section>

        <section class="displayMonths">
                <table style="text-align: center;">
                    <tr>
                        <!-- titre colonnes du tableau -->
                        <th><strong>id</strong></th>
                        <th><strong>nom</strong></th>
                        <th><strong>mois</strong></th>
                    </tr>
                    <?php
                    // connexion base de donnée via PDO
                    $bdd = new PDO('mysql:host=sql202.byetcluster.com;dbname=b22_26110660_biokop;charset=UTF8', 'b22_26110660', 'N94jLk5c');
                    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

                    $moisView = $_POST['mois_view'];
                    
                    $sql = "SELECT id_fl, nom, mois FROM `TBLfruitEtLegume` WHERE mois = '$moisView'";

                    // Affichage des légumes sous forme de tableau pour le mois demandé
                    foreach ($bdd->query($sql) as $row) { ?>
                    <tr>
                        <td name="id_fl"><?php print $row['id_fl'] ?></td>
                        <td><?php print $row['nom'] ?></td>
                        <td><?php print $row['mois'] ?></td>
                    </tr>
                    <?php } ?>
                </table>
                <br>
        </section>

        <section class="create">
            <form action="create.php" method="POST">
                <input type="text" class="input" name="nom" placeholder="Nom">
                <select name="mois" id="mois">
                    <option value="janvier">janvier</option>
                    <option value="fevrier">fevrier</option>
                    <option value="mars">mars</option>
                    <option value="avril">avril</option>
                    <option value="mai">mai</option>
                    <option value="juin">juin</option>
                    <option value="juillet">juillet</option>
                    <option value="aout">aout</option>
                    <option value="septembre">septembre</option>
                    <option value="octobre">octobre</option>
                    <option value="novembre">novembre</option>
                    <option value="decembre">decembre</option>
                </select>
                <br><br>
                <button type="submit">Créer</button>
            </form>
        </section>

        <section class="displayMonths">
            <form action="delete.php" method="POST">
                <label for="name">Supprimer l'ID suivant :</label><br><br>
                <input type="text" name="id_fl" placeholder="id">
                <button type="submit">Supprimer</button>
            </form>
        </section>

        <section class="displayMonths">
            <form action="update.php" method="POST">
                <input type="text" class="input" style="width:3rem;" name="id_fl" placeholder="id">
                <input type="text" class="input" style="width:7rem;" name="nom" placeholder="Nom">
                <select name="mois" id="mois">
                    <option value="janvier">janvier</option>
                    <option value="fevrier">fevrier</option>
                    <option value="mars">mars</option>
                    <option value="avril">avril</option>
                    <option value="mai">mai</option>
                    <option value="juin">juin</option>
                    <option value="juillet">juillet</option>
                    <option value="aout">aout</option>
                    <option value="septembre">septembre</option>
                    <option value="octobre">octobre</option>
                    <option value="novembre">novembre</option>
                    <option value="decembre">decembre</option>
                </select>
                <br><br>
                <button type="submit">Modifier</button>
            </form>
        </section>

    </main>
    
    <?php include('footer.php') ?>
    <script src="daltonien.js"></script>

</body>
</html>