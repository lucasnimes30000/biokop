/* Déclarations des variables requises */
/* "eye" = Logo oeil barré présent dans le header de toutes les pages */
let eye = document.querySelector("#eye");
let header = document.querySelector("#header");
let footer = document.querySelector("#footer");

/* On place un écouteur d'évènement du "eye", de type "click" */
eye.addEventListener("click", event => {

    /* À chaque clique sur "eye", les class "toggle" & "white-eye" se rajouteront au click sur leurs éléments respectifs, et s'enlèveront au re-click */
    header.classList.toggle("toggle");
    footer.classList.toggle("toggle");
    eye.classList.toggle("white-eye")
})