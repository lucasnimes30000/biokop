<?php 
// Je récupère l'information envoyés depuis l'input du formulaire de connexion, ayant pour name "email"
$email = $_POST['email'];
// traiter les "post" / les données utilisateur

// Connexion database
$bdd = new PDO('mysql:host=sql202.byetcluster.com;dbname=b22_26110660_biokop;charset=UTF8', 'b22_26110660', 'N94jLk5c');$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// requête SQL préparée
$req = $bdd->prepare("SELECT id_user, email, pass FROM TBLuser WHERE email = '$email'");
// Exécution requête SQL
$req->execute();
$resultat = $req->fetch();
// Vérification du password hashed dans la DB et du password hashed renseigné dans l'input
$isPasswordCorrect = password_verify($_POST['pass'], $resultat['pass']);
// Vérification si les informations rentrées sont les mêmes qu'en DB
if (!$resultat)
{
    echo 'Mauvais identifiant ou mot de passe ! <a href="log_in.php">Réessayer</a>';
}
else
{
    if ($isPasswordCorrect) {
        // Si tous est OK, on ouvre la session
        session_start();
        $_SESSION['id_user'] = $resultat['id_user'];
        $_SESSION['email'] = $email;
        header('Location: admin.php');
    }
    else {
        echo 'Mauvais - identifiant ou mot de passe ! <a href="log_in.php">Réessayer</a>';
    }
}
?>